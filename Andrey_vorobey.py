from random import randint
file = open(r"D:\Users\skiyazov\Desktop\test.txt", "r")
data = file.read().splitlines()
file.close()

attempts = int(input("Введите количество попыток за которое угадаете слово: "))

word = data[randint(0, len(data) - 1)]
word = word.replace(" ", "")
word = word.upper()
wordList = list(word)
guessWord = ""
guessWordList = list(guessWord.ljust(len(word), "*"))

print("Слово сотоит из ", len(guessWordList), " букв")
for i in range(attempts):
    print("Количество попыток: ", attempts-i)
    s = input("Введите символ или слово целиком: ")
    s = s.upper()
    if len(s) == 0:
        print("Символ не может быть пустым")
    elif len(s) > 1:
        if s == word:
            print("Вы угадали слово, загаданное слово было ", word)
            guessWord = s
            break
        else:
            print("Вы не угадали слово.")
    else:
        for j in range(len(word)):
            if s == wordList[j]:
                guessWordList[j] = s
        guessWord = "".join(guessWordList)
        print(guessWord)
        if guessWord.find("*") == -1:
            print("Вы угадали слово, загаданное слово было ", guessWord)
            break
if guessWord.find("*") != -1:
    print("Вы не угадали слово, загаданное слово было ", word)

